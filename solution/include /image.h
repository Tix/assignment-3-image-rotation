//
// Created by Аркадий Ри on 05.03.2023.
//

#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H
#include <stdint.h>
#include <stdlib.h>



struct  pixel {
    uint8_t b, g, r;
};


struct image {
    size_t width, height;
    struct pixel *data;
};


struct image img_creation(size_t width, size_t height);
void img_destroying(struct image *image);

#endif //IMAGE_TRANSFORMER_IMAGE_H
