//
// Created by Аркадий Ри on 10.02.2023.
//

#ifndef IMAGE_TRANSFORMER_IMG_ROTATION_H
#define IMAGE_TRANSFORMER_IMG_ROTATION_H


#include <stdint.h>
#include <stdlib.h>

struct image rotation(struct image input_img);

#endif

