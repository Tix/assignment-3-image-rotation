#ifndef BMP_FORMAT_H
#define BMP_FORMAT_H

#include <stdint.h>
#include <stdio.h>

#include "img_rotation.h"




struct __attribute__((packed))  bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_HEADER,
    READ_INVALID_BODY,
    READ_INCORRECT_PADDING

};

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR_HEADER,
    WRITE_ERROR_PADDING,
    WRITE_ERROR_BODY
};

enum read_status check_BMP_header(FILE *file, struct bmp_header *check_header);

enum read_status get_body(FILE *file, struct image *img);

struct image *get_image(struct bmp_header header);

enum write_status write_body(FILE *outfile, struct image *img);

enum write_status write_header(FILE *outfile, struct image *img);

#endif
