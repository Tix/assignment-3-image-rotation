//
// Created by Аркадий Ри on 12.02.2023.
//
#include "../include /image.h"

struct image rotation(struct image input_img) {

    struct image output_img = img_creation(input_img.height, input_img.width); //invert parameters

    for (uint64_t i = 0; i < output_img.height * output_img.width; i++) {
        uint64_t future_file_line = i / output_img.width;
        uint64_t future_file_column = i % output_img.width;
        struct pixel output_pixel = input_img.data[input_img.width * (input_img.height - future_file_column - 1) +
                                                   future_file_line];
        output_img.data[i] = output_pixel;
    }

    return output_img;
}
