//
// Created by Аркадий Ри on 10.02.2023.
//
#include "../include /img_rotation.h"
#include "../include /image.h"
#include <stdlib.h>


struct image img_creation(size_t width, size_t height) {
    struct image image = {0};
    image.width = width;
    image.height = height;
    image.data = malloc(3 * width * height);

    return image;
}

void img_destroying(struct image *image) {
    free(image->data);
}
