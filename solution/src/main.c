#include "../include /img_rotation.h"
#include "../include /bmp_format.h"
#include "../include /image.h"
#include <stdio.h>

int main(int argc, char **argv) {

    if (argc != 3) {
        fprintf(stderr, "Invalid number of arguments\n");
        return 1;
    }

    FILE *input_file = fopen(argv[1], "r");
    FILE *output_file = fopen(argv[2], "w");

    if (input_file == NULL) {
        fprintf(stderr, "Can't open source file\n");
        return 1;
    }
    if (output_file == NULL) {
        fprintf(stderr, "Can't open target file\n");
        return 1;
    }
    struct bmp_header header = {0};

    enum read_status header_status = check_BMP_header(input_file, &header);
    if (header_status == READ_INVALID_HEADER) {
        fprintf(stderr, "Invalid header of bmp file\n");
        return 1;
    }
    if (header_status == READ_INVALID_SIGNATURE) {
        fprintf(stderr, "Invalid signature of file\n");
        return 1;
    }

    struct image *row_image = get_image(header);

    if (row_image == NULL) {
        fprintf(stderr, "Failed image creation\n");
        return 1;
    }
    enum read_status body_status = get_body(input_file, row_image);

    if (body_status == READ_INVALID_BODY) {
        fprintf(stderr, "Unsuccessful reading of file body\n");
        return 1;
    }
    struct image final_image = rotation(*row_image);

    printf("Work with image done!\n");

    enum write_status write_header_status = write_header(output_file, &final_image);

    if (write_header_status == WRITE_ERROR_HEADER) {
        fprintf(stderr, "Unsuccessful writing header in file\n");
        return 1;
    }

    enum write_status write_body_status = write_body(output_file, &final_image);

    if (write_body_status == WRITE_ERROR_BODY) {
        fprintf(stderr, "Unsuccessful writing body in file\n");
        return 1;
    }

    if (write_body_status == WRITE_ERROR_PADDING) {
        fprintf(stderr, "Problem with padding\n");
        return 1;
    }

    printf("Work completed!\n");
    img_destroying(row_image);
    free(row_image);
    img_destroying(&final_image);
    fclose(input_file);
    fclose(output_file);
}
