//
// Created by Аркадий Ри on 10.02.2023.
//
#include "../include /bmp_format.h"
#include "../include /image.h"

#include <stdint.h>
#include <stdlib.h>

const uint16_t BMP_TYPE = 0x4D42; //"BM" in ASCII
const uint16_t BI_PLANES = 1;
const uint16_t BI_BIT_COUNT = 24;
const uint32_t F_SIZE = 40; // sizeof(struct bmp_header) - 14, 14 - is size of header in bmp format files
const uint32_t BMP_COMPRESSION = 0;
const uint32_t B_OFF_BITS = 54;


static int64_t get_padding(uint64_t width) {
    int64_t padding = 0;
    if (((width) * 3) % 4 != 0) {
        return (int64_t) (4 - (((width) * 3) % 4));
    }
    return padding;
}


static struct bmp_header set_header(struct image *img) {

    struct bmp_header input_head={0};

    input_head.bfType = BMP_TYPE;
    input_head.biSize = F_SIZE;
    input_head.bOffBits = B_OFF_BITS;
    input_head.biPlanes = BI_PLANES;
    input_head.biBitCount = BI_BIT_COUNT;
    input_head.biHeight = img->height;
    input_head.biWidth = img->width;
    input_head.biCompression = BMP_COMPRESSION;
    input_head.biSizeImage =
            (img->width * 3 * img->height + get_padding(img->width)) + img->height * get_padding(img->width);
    input_head.bfileSize =
            (img->width * 3 * img->height + get_padding(img->width)) + img->height * get_padding(img->width) +
            get_padding(img->width);
    input_head.bfReserved = 0;
    input_head.biXPelsPerMeter = 0;
    input_head.biYPelsPerMeter = 0;
    input_head.biClrUsed = 0;
    input_head.biClrImportant = 0;
    return input_head;
}

enum read_status check_BMP_header(FILE *file, struct bmp_header *check_header) {
    if (fread(check_header, sizeof(struct bmp_header), 1, file) != 1) {
        return READ_INVALID_HEADER;
    }
    if (check_header->bfType != BMP_TYPE) {
        return READ_INVALID_SIGNATURE;
    }
    return READ_OK;

}

struct image *get_image(struct bmp_header income_header) {

    struct image *image;

    image = malloc(sizeof(struct image));
    *image = img_creation(income_header.biWidth, income_header.biHeight);

    return image;

}

enum read_status get_body(FILE *file, struct image *img) {
    for (uint64_t i = 0; i < img->height; i++) {
        if (fread(img->data + img->width * i, sizeof(struct pixel), img->width, file) != img->width) {

            return READ_INVALID_BODY;
        }

        if (fseek(file, (long) get_padding(img->width), SEEK_CUR) != 0) {
            return READ_INCORRECT_PADDING;
        }
    }
    return READ_OK;
}

enum write_status write_header(FILE *outfile, struct image *img) {
    struct bmp_header out_head = set_header(img);
    if (fwrite(&out_head, B_OFF_BITS, 1, outfile) != 1) {
        return WRITE_ERROR_HEADER;
    }
    return WRITE_OK;
}

enum write_status write_body(FILE *outfile, struct image *img) {
    int64_t padding = get_padding(img->width);
    uint8_t padding_value = 0;

    for (uint64_t i = 0; i < img->height; i++) {
        if (fwrite(img->data + img->width * i, 3 * img->width, 1, outfile) != 1) {
            return WRITE_ERROR_BODY;
        }
        for (int64_t j = 0; j < padding; j++) {
            if (fwrite(&padding_value, sizeof(uint8_t), 1, outfile) != 1) {
                return WRITE_ERROR_PADDING;
            }
        }


    }

    return WRITE_OK;
}


